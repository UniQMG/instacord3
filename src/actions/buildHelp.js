function buildHelp(router, substr, msg, actions) {
  /**
   * Generates help text from router provided by closure
   * @param {string} query
   * @returns {string} helptext
   */
  return async function help(query="") {
    // Find sub routers first
    for (let command of router.commands) {
      if (query.length > 0 && query.startsWith(command.name)) {
        let mnt = router.mountCommands.get(command);
        if (mnt) {
          let subquery = query.substring(command.name.length).trim();
          return buildHelp(mnt, substr, msg, actions)(subquery);
        } else if (query.trim() == command.name) {
          let { name, usage } = command;
          return `${name}\n${'-'.repeat(name.length)}\n${usage}`
        }
      }
    }

    // Generate help for the given router
    let lines = router.commands.map(command => {
      if (!command.name) return null;
      let mnt = router.mountCommands.get(command);
      let usage = command.usage || '';

      let eol = usage.indexOf('\n');
      if (eol == -1) eol = usage.length;
      let firstline = usage.substring(0, eol) || '';

      return [command.name, ' | ', firstline || (mnt && '{subgroup}') || '']
    }).filter(x => x);

    // Collate and pad lines so they line up
    let maxline = [];
    for (var i = 0; i < lines.length; i++)
      for (var u = 0; u < lines[i].length; u++)
        maxline[u] = Math.max(lines[i][u].length, maxline[u] || 0);

    return lines.map(line => {
      for (var u = 0; u < line.length-1; u++) {
        let padding = maxline[u] - line[u].length;
        line[u] += " ".repeat(padding);
      }
      return line.join('');
    }).join('\n');
  }
}
module.exports = buildHelp;
