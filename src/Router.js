const Command = require('./Command');
const buildHelp = require('./actions/buildHelp');

/**
 * Routes commands and stuff
 */
class Router {
  /**
   * Creates a new Router
   * @param {?Object} options
   * @param {?boolean} options.passthrough
   * whether commands can ignore group resolvers (so 'foo bar' can be matched
   * by either 'foo bar' or just 'bar'). Not applicable to help generation.
   * Only applies to group names set using {@link Router#group} and not using
   * any custom resolvers.
   */
  constructor(options={}) {
    /** @type boolean */
    this.passthrough = options.passthrough || false;

    /** @type {Command[]} */
    this.commands = [];

    /**
     * Maintains a mapping from commands to routers mounted on those commands.
     * Used for helptext generation.
     * @type {Map.<Command, Router>}
     */
    this.mountCommands = new WeakMap();

    /** @type {CommandError[]} */
    this.errorHandlers = []
  }

  /**
   * @param {(string|CommandArguments|Command)} args
   * @param {?CommandExecutor} executor
   * the executor to use, overriding any specified in the CommandArguments
   * @returns {Router} this, chainable
   */
  command(args, executor) {
    if (typeof args == 'string' || args instanceof String)
      args = { name: args, exec: executor };
    if (executor) args.exec = executor;
    let command = Command.from(args);
    this.commands.push(command);
    return this;
  }

  /**
   * Mounts another router on this one, as a command subgroup
   * @param {(CommandArguments|Command)} args command to access the subgroup
   * @param {Router} router the router to mount
   * @returns {Router} this, chainable
   */
  mount(args, router) {
    let command = Command.from(args);
    this.commands.push(command);
    command.addCallback(router.resolve.bind(router));
    this.mountCommands.set(command, router);
    return this;
  }

  /**
   * Creates a command subgroup with a given name and resolver
   * @param {string} name name for the subgroup command
   * @param {...CommandResolver} resolvers resolvers used for the subgroup command
   * @returns {Router} the created subgroup
   */
  group(name, ...resolvers) {
    if (resolvers.length == 0) resolvers.push(substr => {
      if (substr.startsWith(name)) return name;
      if (this.passthrough) return 0;
      return false;
    });
    let command = Command.from({ name, cmd: resolvers, exec: null });
    let router = new Router();
    this.mount(command, router);
    return router;
  }

  /**
   * Resolves a command, executing relevent commands. Promise may reject if no
   * {@link Router#onError} callbacks are provided.
   * @param {String} substr the string to use for resolving.
   * @param {external:Message} msg the discord message being parsed
   * @param {Actions} actions
   */
  async resolve(substr, msg, actions={}) {
    actions = Object.assign({}, actions);
    Object.defineProperty(actions, 'help', {
      configurable: true,
      enumerable: true,
      get: () => buildHelp(this, substr, msg, actions),
    });

    for (let command of this.commands) {
      try {
        let match = await command.test(substr, msg, actions);
        if (!match || match.length === undefined) continue;

        if (actions._permissions) {
          let [result, failedPermission] = await command.testPermissions(
            substr, msg, actions, actions._permissions
          );
          if (!result) {
            if (actions._permissionCheckFailed)
              actions._permissionCheckFailed(command, failedPermission);
            continue;
          }
        }

        await command.exec(substr, msg, actions);
      } catch(ex) {
        if (this.errorHandlers.length > 0) {
          for (let handler of this.errorHandlers)
            await handler(ex, substr, msg, actions);
          return;
        }
        throw ex;
      }
    }
  }


  /**
  * A CommandError is a callback for when a command throws an error.
  * @callback CommandError
  * @param {Error} error the thrown error
  * @param {string} substr the remaining unparsed substring
  * @param {external:Message} msg the discord message being parsed
  * @param {Actions} actions
  * @async
  */

  /**
   * Catches an async error
   * @param {CommandError} callback
   */
  onError(callback) {
    this.errorHandlers.push(callback);
  }
}

module.exports = Router;
