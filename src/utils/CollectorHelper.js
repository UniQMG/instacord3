class CollectorHelper {
  /**
   * Collects a certain number of reactions
   * @param {external:Message} msg message to collect reactions on
   * @param {?external:User} user user whom is reacting, null for anyone
   * @param {Number} limit number of reactions to collect
   * @param {Number} timeout how long to collect reactions for
   * @example
   * let reply = await msg.channel.send("React to me!");
   * let handler = new CollectorHelper(reply, msg.author)
   *   .collect('✅', async () => { await reply.edit('Yes!'); })
   *   .collect('❌', async () => { await reply.edit('No...'); })
   *   .onTimeout(async () => { await reply.edit('Please respond!'); })
   * await handler.start();
   * // this will be reached after the message reply is edited
   */
  constructor(msg, user, limit=1, timeout=15000) {
    this.msg = msg;
    this.user = user;
    this.limit = limit;
    this.timeout = timeout;
    this.callbacks = {};
    this.timeoutCallbacks = [];
    this.reactions = [];
  }

  /**
   * Callback for when a reaction is collected. The reaction is automatically
   * added to the message. Only reactions with callback(s) will count towards
   * the collection limit.
   * @param {String} reaction emoji string
   * @param {Function} callback
   * @returns {CollectorHelper} this (for chainability)
   */
  collect(reaction, callback) {
    if (!this.callbacks[reaction]) {
      this.callbacks[reaction] = [];
      this.reactions.push(reaction);
    }
    this.callbacks[reaction].push(callback);
    return this;
  }

  /**
   * Callback for if the reaction collector times out before the collection
   * limit is reached.
   * @param {Function} callback
   * @returns {CollectorHelper} this (for chainability)
   */
  onTimeout(callback) {
    this.timeoutCallbacks.push(callback);
    return this;
  }

  /**
   * Starts the collector, returning a promise that resolves once
   * the limit is reached or it times out.
   */
  async start() {
    for (let reaction of this.reactions)
      await this.msg.react(reaction);

    let collector = this.msg.createReactionCollector((reaction, user) => {
      if (user.id == this.msg.client.user.id) return false;
      return this.user && this.user.id
        ? user.id == this.user.id
        : true;
    }, { time: this.timeout });

    await new Promise(async (resolve, reject) => {
      var collected = 0;
      collector.on('collect', async r => {
        try {
          if (collected >= this.limit) return;
          let cbs = this.callbacks[r.emoji.name];
          if (cbs.length > 0) {
            collected++;
            for (let cb of cbs) await cb();
            if (collected >= this.limit) resolve();
          }
        } catch(ex) { reject(ex) }
      });
      collector.on('end', async c => {
        try {
          if (collected < this.limit) {
            for (let cb of this.timeoutCallbacks) await cb();
          }
          resolve();
        } catch(ex) { reject(ex) }
      });
    });
  }
}

module.exports = CollectorHelper;
