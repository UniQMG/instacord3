const Discord = require('discord.js');
const Router = require('./Router');
const { Client } = Discord;
const { EventEmitter } = require('events');

/**
 * Instacord
 * @emits {external:Message, Command} permissionError
 */
class Instacord extends EventEmitter {
  /**
   * Creates a new Instacord instance
   */
  constructor() {
    super();
    /** @property {external:Client} client the discord.js client */
    this.client = new Client();
    /** @property {Router} router the main router to add your commands to */
    this.router = new Router();
    /** @property {Map} permissions */
    this.permissions = new Map();

    this.client.on('message', msg => {
      this.router.resolve(msg.content, msg, {
        _permissions: this.permissions,
        _permissionCheckFailed: (cmd, perm) => {
          this.emit('permissionError', msg, cmd, perm);
        }
      });
    });
  }

  /**
   * Defines a permission and a resolver
   * @param {String} name
   * @param {Function<Promise<boolean>, String substr, external:Message, Object, Command, String failedPermission>} resolver
   */
  permission(name, resolver) {
    this.permissions.set(name, resolver);
  }

  static get Router() {
    return require('./Router');
  }

  static get Command() {
    return require('./Command');
  }

  static get CollectorHelper() {
    return require('./utils/CollectorHelper');
  }

  /**
   * Logs the discord.js client into discord
   * @param {string} token Your bot token
   */
  login(token) {
    this.client.login(token);
  }
}

module.exports = Instacord;
