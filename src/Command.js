var uid = 0;

/**
 * Handles command execution
 */
class Command {
  /**
   * A CommandResolver is an object that describes if and how a string
   * should match a command, as well as how much of it matches. You can pass an
   * exact string to match, a regular expression, or a function.
   * Usage notes:
   * - The matched text is removed from the substring in descendent commands.
   * - Regular expressions will consume up to and including their matched text.
   * - Text returned from a function is only used for length.
   * @typedef {(string|RegExp|CommandResolverFunction)} CommandResolver
   */

  /**
   * A CommandResolverFunction is given a string and returns how much of that
   * string to match. A falsey return value indicates no match, while a number
   * or a string's length is used to match a certain number of characters.
   * You should always match against the given substring, not the discord
   * message content.
   * <br>Preferably a pure function, though not a requirement.
   * @callback CommandResolverFunction
   * @param {string} substr the string to attempt a match against
   * @param {external:Message} msg the discord message being parsed
   * @param {Actions} actions
   * @returns {?(string|number)}
   * @async
   */

   /**
   * A CommandExecutor is like a {@link CommandResolverFunction} but without a
   * return type. The recommendation to be a pure function also does not apply.
   * @callback CommandExecutor
   * @param {string} substr the string to attempt a match against
   * @param {external:Message} msg the discord message being parsed
   * @param {Actions} actions
   * @async
   */

   /**
    * User-defined object, for storing whatever.
    * @typedef {object} Actions
    */

   /**
    * Creates a new Command
    */
   constructor() {
     /** @type {Number} a unique number associated with the object instance */
     this.uid = ++uid;
     /** @type {?string} */
     this.name = null;
     /** @type {?string} */
     this.usage = null;

     /** @type {CommandResolver[]} */
     this.resolvers = [];
     /** @type {CommandExecutor[]} */
     this.callbacks = [];
     /** @type {String[]} */
     this.permissions = [];
     /** @type {String} 'AND' or 'OR' */
     this.permissionsRule = 'AND';
   }

   /**
    * Defines a quick way of creating a command from a raw object
    * If nothing is specified for cmd, name is used as the default.
    * @typedef {Object} CommandArguments
    * @property {string} name the name of the argument, and default resolver
    * @property {?string} usage the command usage
    * @property {?(CommandResolver|CommandResolver[])} cmd
    *   The CommandResolver(s) to use to test
    * @property {?CommandExecutor} exec the command to execute
    */

   /**
    * Parses a command from a CommandArguments object.
    * Returns the same Command if a Command is passed.
    * @param {(CommandArguments|Command)} cmdargs
    * @return {Command}
    */
   static from(cmdargs) {
     if (cmdargs instanceof Command) return cmdargs;
     let { name, usage, permissions, cmd, exec, data } = cmdargs;

     let command = new Command();
     command.setName(name);
     command.setUsage(usage);
     if (permissions) {
       if (Array.isArray(permissions)) {
         command.permissions.push(...permissions);
       } else {
         command.permissions.push(permissions);
       }
     }
     if (cmd && cmd.forEach) {
       cmd.forEach(_cmd => command.addResolver(_cmd));
     } else if (cmd) {
       command.addResolver(cmd);
     } else {
       command.addResolver(name);
     }
     if (exec) command.addCallback(exec);
     if (data) Object.assign(command, data);
     return command;
   }

   /**
    * Sets the name of the Command
    * @param {String} name the new name for the command
    * @returns {Command} this, chainable
    */
   setName(name) {
     this.name = name;
     return this;
   }

   /**
    * Sets the usage text for the command
    * @param {String} usage the new usage text for the command
    * @returns {Command} this, chainable
    */
   setUsage(usage) {
     this.usage = usage;
     return this;
   }

   /**
    * Adds a resolver to the function. Resolvers are executed in order, and
    * can exit prematurely if one fails.
    * @param {CommandResolver} resolver
    * @returns {Command} this, chainable
    */
   addResolver(resolver) {
     this.resolvers.push(resolver);
     return this;
   }

   /**
    * Adds a callback to the function. Callbacks are executed in order once the
    * command matches.
    * @param {CommandExecutor} callback
    * @returns {Command} this, chainable
    */
   addCallback(callback) {
     this.callbacks.push(callback);
     return this;
   }

   /**
    * Tests how much of a substring this command supports
    * @param {string} substr the string to attempt a match against
    * @param {external:Message} msg the discord message being parsed
    * @param {Actions} actions
    * @returns {(string|boolean)} the matched substring, or false for no match.
    */
   async test(substr, msg, actions) {
     let originalSubstr = substr;
     substr = substr.trim();
     for (let resolver of this.resolvers) {
       if (resolver instanceof RegExp) {
         let match = resolver.exec(substr);
         if (!match) return false;
         substr = substr.substring(match[0].length + match.index).trim();
       }
       if (typeof resolver == 'function') {
         let match = await resolver(substr, msg, actions);
         if (match !== 0 && match !== '' && !match) return false;
         substr = substr.substring(match.length || match).trim();
       }
       if (typeof resolver == 'string' || resolver instanceof String) {
         if (!substr.startsWith(resolver)) return false;
         substr = substr.substring(resolver.length).trim();
       }
     }
     return originalSubstr.substring(0, originalSubstr.length - substr.length);
   }


   /**
    * Tests if the command permissions are available for the given permission hash
    * @param {string} substr the string to attempt a match against
    * @param {external:Message} msg the discord message being parsed
    * @param {Actions} actions
    * @param {Map<String, Function>} permissions
    * @returns {(string|boolean)} the matched substring, or false for no match.
    */
  async testPermissions(substr, msg, actions, permissions) {
    for (let permissionName of this.permissions) {
      let resolver = permissions.get(permissionName);
      let result = resolver
        ? await resolver(substr, msg, actions, this)
        : [false, permissionName];

      if (this.permissionsRule == 'OR' && result)
        return [true];

      if (this.permissionsRule == 'AND' && !result)
        return [false, permissionName];
    }

    if (this.permissionsRule == 'AND')
      return [true];
    return [false, this.permissions[0]];
  }

  /**
  * Executes the command, running all callbacks.
  * @param {string} substr the string to attempt a match against
  * @param {external:Message} msg the discord message being parsed
  * @param {Actions} actions
  * @returns {(string|boolean)} the consumed substring, or false for no match.
  */
  async exec(substr, msg, actions) {
    let match = await this.test(substr, msg, actions);
    if (match.length === undefined && !match) return false;
    substr = substr.substring(match.length);
    for (let callback of this.callbacks)
      await callback(substr, msg, actions);
    return substr;
  }
}

module.exports = Command;
