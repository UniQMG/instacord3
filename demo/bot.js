const token = require('./secret.js');
const Instacord = require('..');

const instacord = new Instacord();
instacord.login(token);

/* Allows commands to match in groups without specifying the group name */
/* Not recommended for a large number of subgrouped commands */
instacord.router.passthrough = true;

instacord.client.on('ready', () => {
  console.log("Ready!");
});

instacord.router.group('general')
  .command({
    name: 'foo',
    usage: 'prints bar' +
    '\nmay also on occasion print baz'
  }, async (sub, msg, actions) => {
    await msg.channel.send(Math.random() > 1/10 ? 'bar' : 'baz');
  })
  // command calls are chainable, though you can also store the router returned
  // by group in a variable.
  .command('reactdemo', async (sub, msg, actions) => {
    let reply = await msg.channel.send("React to me!");
    let handler = new Instacord.CollectorHelper(reply, msg.author)
      .collect('✅', async () => { await reply.edit('Yes!'); })
      .collect('❌', async () => { await reply.edit('No...'); })
      .onTimeout(async () => { await reply.edit('Please respond!'); })
    await handler.start();
    await msg.channel.send("Bye!");
  });

instacord.router.command({
  name: 'help',
  usage: 'help <command>' +
  '\nhelp lists the commands in a given group, or lists' +
  '\nthe extended usage when used on a specific command' +
  '\nFor example, `help general` would print the commands' +
  '\nin the general group, and `help general foo` would' +
  '\nprint advanced usage for the foo command.'
}, async (sub, msg, actions) => {
  await msg.channel.send("```\n" + await actions.help(sub) + "```");
});

instacord.router.onError(async (error, substr, msg, actions) => {
  await msg.channel.send("Error processing command! ```" + error.stack + "```");
});
